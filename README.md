En este proyecto se incluyen ejemplos de uso de esta sdk en los junits de la clase BraintreeTest. 
Indico algunas observaciones despu�s de una primera lectura vertical de la documentaci�n.

## Integraci�n

### Hello World
* [Android] (https://developers.braintreepayments.com/android+java/start/hello-client)
* [IOS] (https://developers.braintreepayments.com/ios+java/start/hello-client)
* [BackEnd Java] (https://developers.braintreepayments.com/android+java/start/hello-server)

### Maven


### Repositorios:

     Maven Central, which should be enabled by default. No additional repositories are required.

### Dependencias

    <dependency>
      <groupId>com.braintreepayments.gateway</groupId>
      <artifactId>braintree-java</artifactId>
      <version>PUT VERSION NUMBER HERE</version>
    </dependency>

### Puntos negativos:

 * El proceso de creaci�n de un seller no es sincrono, se crea el seller pero despu�s nos env�an si hay notificaci�n de si ha ido bien.
Tendr�amos que mirar como encajarlo.

* En el proceso de a�adir una cuenta bancaria no validan que sea correcta, si no lo �s fallar� despu�s. De hecho un "submerchant" tiene
 como mucho una cuenta asociada, si queremos que tenga m�s las tendremos que guardar nosotros en base de datos.
 
 * Te�ricamente se pueden recibir pagos para 130+ diferentes monedas, e ingresar pagos en 13 monedas aunque ser�a necesario
 crear un "merchant" para cada moneda. A la pr�ctica, a d�a de hoy "Braintree's Marketplace is only for US-domiciled merchants accepting
 USD transactions on cards"

## M�todos de pago

* Apple Pay: Solo para IOS en USA
* Tarjetas de Credito:Visa, Mastercard, Maestro, American Express, Discover, JCB
* Venmo: Ios y Android, solo USA.
* SEPA Direct Debit (beta): S�lo Alemania de momento.


## Herramientas Fraude

Incorpora algunas herramientas para prevenir el fraude:

 * [Fraud Tools Overview](https://articles.braintreepayments.com/guides/fraud-tools/overview)
 * 3D Secure. Por defecto lo tienen deshabilitado, aunque existe la posibilidad de habilitarlo si se cree conveniente.

## Documentaci�n

La documentaci�n parece muy completa:

 * [Official documentation](https://developers.braintreepayments.com/)

### Dashboard/Sandbox

El dashboard es muy completo, tiene muchas funcionalidades:

* [Sandbox](https://sandbox.braintreegateway.com/login)
*  User: wallapopdev
*  Password: DevBra�ntree$2015