import com.braintreegateway.BraintreeGateway;
import com.braintreegateway.Environment;
import com.braintreegateway.Result;
import com.braintreegateway.Transaction;
import com.braintreegateway.TransactionRequest;
import com.braintreegateway.ValidationError;

import java.math.BigDecimal;

/**
 * Created by Dimas-Pedraza on 22/06/2015.
 */
public class BraintreeExample {

    public static void main(String[] args) {
        BraintreeGateway gateway = new BraintreeGateway(
                Environment.SANDBOX,
                "h2prd5ttfwg8fjj9",
                "2gv43584nr74y2s5",
                "eacc94193aa3eee99a6734ada153c5ce"
        );

        TransactionRequest request = new TransactionRequest().
                amount(new BigDecimal("1000.00")).
                creditCard().
                number("4111111111111111").
                expirationDate("05/2009").
                done();

        Result<Transaction> result = gateway.transaction().sale(request);

        if (result.isSuccess()) {
            Transaction transaction = result.getTarget();
            System.out.println("Success!: " + transaction.getId());
        } else if (result.getTransaction() != null) {
            Transaction transaction = result.getTransaction();
            System.out.println("Error processing transaction:");
            System.out.println("  Status: " + transaction.getStatus());
            System.out.println("  Code: " + transaction.getProcessorResponseCode());
            System.out.println("  Text: " + transaction.getProcessorResponseText());
        } else {
            for (ValidationError error : result.getErrors().getAllDeepValidationErrors()) {
                System.out.println("Attribute: " + error.getAttribute());
                System.out.println("  Code: " + error.getCode());
                System.out.println("  Message: " + error.getMessage());
            }
        }
    }
}

