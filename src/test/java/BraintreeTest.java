import com.braintreegateway.BraintreeGateway;
import com.braintreegateway.ClientTokenRequest;
import com.braintreegateway.Customer;
import com.braintreegateway.CustomerRequest;
import com.braintreegateway.Environment;
import com.braintreegateway.MerchantAccount;
import com.braintreegateway.MerchantAccountRequest;
import com.braintreegateway.PaymentMethod;
import com.braintreegateway.PaymentMethodRequest;
import com.braintreegateway.Result;
import com.braintreegateway.Transaction;
import com.braintreegateway.TransactionRequest;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Random;

/**
 * Created by Dimas-Pedraza on 22/06/2015.
 */
public class BraintreeTest {

    private final static String MERCHANT_ID = "bfs33v8ny4gn38cj";
    BraintreeGateway gateway = new BraintreeGateway(
            Environment.SANDBOX,
            "h2prd5ttfwg8fjj9",
            "2gv43584nr74y2s5",
            "eacc94193aa3eee99a6734ada153c5ce"
    );
    private String customerId;


    @Test
    public void testAddCustomerWithPaymentMethod() throws Exception {
        String sRandom = Integer.toString(new Random().nextInt(1000000000) + 1);
        CustomerRequest request = new CustomerRequest()
                .firstName("Fred " + sRandom)
                .lastName("Jones").id(sRandom).email(sRandom + "@wallapop.com")
                .paymentMethodNonce("nonce-from-the-client");

        Result<Customer> result = gateway.customer().create(request);

        Assert.assertTrue(result.isSuccess());

        Assert.assertEquals(sRandom, result.getTarget().getId());

        customerId = sRandom;
    }


    @Test
    public void testAddPaymentMethod() throws Exception {

        if (customerId == null) {
            testAddCustomerWithPaymentMethod();
        }
        PaymentMethodRequest request = new PaymentMethodRequest()
                .customerId(customerId)
                .paymentMethodNonce("nonce-from-the-client").options().verifyCard(true).done();

        Result<? extends PaymentMethod> result = gateway.paymentMethod().create(request);

        Assert.assertTrue(result.isSuccess());

    }

    @Test
    public void testCreateClientToken() {
        ClientTokenRequest clientTokenRequest = new ClientTokenRequest();
        //.customerId(aCustomerId);
        String token = gateway.clientToken().generate(clientTokenRequest);

        Assert.assertNotNull(token);
    }

    @Test
    public void testCreateSubMerchantForSeller() {
        String sRandom = Integer.toString(new Random().nextInt(1000000000) + 1);
        MerchantAccountRequest request = new MerchantAccountRequest().
                individual().
                firstName("Jane ").
                lastName("Doe").
                email("jane@14ladders.com").
                dateOfBirth("1981-11-19").
                address().
                streetAddress("111 Main St").
                locality("Chicago").
                region("IL").
                postalCode("60622").
                done().done().
                funding().
                descriptor("Blue Ladders").
                destination(MerchantAccount.FundingDestination.BANK).
                accountNumber("1123581321113").
                routingNumber("071101307").
                done().
                tosAccepted(true).
                masterMerchantAccountId(MERCHANT_ID).
                id(sRandom);

        Result<MerchantAccount> result = gateway.merchantAccount().create(request);

        Assert.assertTrue(result.isSuccess());

        MerchantAccount ma = result.getTarget();

        Assert.assertNotNull(ma);
        //the sub-merchant account creation is pending
        Assert.assertTrue(ma.getStatus() == MerchantAccount.Status.PENDING);

        Assert.assertEquals(sRandom, ma.getId());

    }

    @Test
    public void testCreateTransactionsWithOutEscrow() {

        String subMerchantAccount = "junit_sub_merchant_account";
        try {
            MerchantAccount merchant_account = gateway.merchantAccount().find(subMerchantAccount);
        } catch (com.braintreegateway.exceptions.NotFoundException e) {

            MerchantAccountRequest request = new MerchantAccountRequest().
                    individual().
                    firstName("Jane ").
                    lastName("Doe").
                    email("jane@14ladders.com").
                    dateOfBirth("1981-11-19").
                    address().
                    streetAddress("111 Main St").
                    locality("Chicago").
                    region("IL").
                    postalCode("60622").
                    done().done().
                    funding().
                    descriptor("Blue Ladders").
                    destination(MerchantAccount.FundingDestination.BANK).
                    accountNumber("1123581321113").
                    routingNumber("071101307").
                    done().
                    tosAccepted(true).
                    masterMerchantAccountId(MERCHANT_ID).
                    id(subMerchantAccount);

            Result<MerchantAccount> result = gateway.merchantAccount().create(request);

            Assert.assertTrue(result.isSuccess());
        }

        TransactionRequest request = new TransactionRequest()
                .merchantAccountId(subMerchantAccount).
                        amount(new BigDecimal("100.00"))
                .paymentMethodNonce("nonce-from-the-client").
                        options().
                        submitForSettlement(true).
                        holdInEscrow(false).done()
                .serviceFeeAmount(new BigDecimal("10.00"));

        Result<Transaction> result = gateway.transaction().sale(request);

        Assert.assertTrue(result.isSuccess());
    }

}
